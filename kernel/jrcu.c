/*
 * Joe's tiny RCU, for small SMP systems.
 *
 * See Documentation/RCU/jrcu.txt for theory of operation and design details.
 *
 * Author: Joe Korty <joe.korty@ccur.com>
 *
 * Acknowledgements: Paul E. McKenney's 'TinyRCU for uniprocessors' inspired
 * the thought that there could could be something similiarly simple for SMP.
 * The rcu_list chain operators are from Jim Houston's Alternative RCU.
 *
 * Copyright Concurrent Computer Corporation, 2011-2012.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <linux/bug.h>
#include <linux/smp.h>
#include <linux/slab.h>
#include <linux/ctype.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/percpu.h>
#include <linux/stddef.h>
#include <linux/string.h>
#include <linux/preempt.h>
#include <linux/uaccess.h>
#include <linux/compiler.h>
#include <linux/irqflags.h>
#include <linux/rcupdate.h>

/*
 * Define an rcu list type and operators.  This differs from linux/list.h
 * in that an rcu list has only ->next pointers for the chain nodes; the
 * list head however is special and has pointers to both the first and
 * last nodes of the chain.  Tweaked so that null head, tail pointers can
 * be used to signify an empty list.
 */
struct rcu_list {
	struct rcu_head *head;
	struct rcu_head **tail;
	int count;		/* stats-n-debug */
};

static inline void rcu_list_init(struct rcu_list *l)
{
	l->head = NULL;
	l->tail = NULL;
	l->count = 0;
}

/*
 * Add an element to the tail of an rcu list
 */
static inline void rcu_list_add(struct rcu_list *l, struct rcu_head *h)
{
	if (unlikely(l->tail == NULL))
		l->tail = &l->head;
	*l->tail = h;
	l->tail = &h->next;
	l->count++;
	h->next = NULL;
}

/*
 * Append the contents of one rcu list to another.  The 'from' list is left
 * corrupted on exit; the caller must re-initialize it before it can be used
 * again.
 */
static inline void rcu_list_join(struct rcu_list *to, struct rcu_list *from)
{
	if (from->head) {
		if (unlikely(to->tail == NULL)) {
			to->tail = &to->head;
			to->count = 0;
		}
		*to->tail = from->head;
		to->tail = from->tail;
		to->count += from->count;
	}
}

/* End of generic rcu list definitions, start of specific JRCU stuff */

struct rcu_data {
	u16 wait;		/* goes false when this cpu consents to
				 * the retirement of the current batch */
	struct rcu_list clist;	/* current callback list */
	struct rcu_list plist;	/* previous callback list */
	raw_spinlock_t lock;	/* protects the above callback lists */
	s64 nqueued;		/* #callbacks queued (stats-n-debug) */
} ____cacheline_aligned_in_smp;

static struct rcu_data rcu_data[NR_CPUS];

/* debug & statistics stuff */
static struct rcu_stats {
	unsigned npasses;	/* #passes made */
	unsigned nlast;		/* #passes since last end-of-batch */
	unsigned nbatches;	/* #end-of-batches (eobs) seen */
	atomic_t nbarriers;	/* #rcu barriers processed */
	atomic_t nsyncs;	/* #rcu syncs processed */
	s64 ninvoked;		/* #invoked (ie, finished) callbacks */
	unsigned nforced;	/* #forced eobs (should be zero) */
} rcu_stats;

#define RCU_HZ_DEFAULT			(20)
#define RCU_HZ_EXPEDITED		(200)
#define RCU_HZ_FASTEST			(210)

static int rcu_period_us = USEC_PER_SEC / RCU_HZ_DEFAULT;
static const int rcu_period_us_expedited = USEC_PER_SEC / RCU_HZ_EXPEDITED;
static const int rcu_period_ns_min = NSEC_PER_SEC / RCU_HZ_FASTEST;

int rcu_scheduler_active __read_mostly;

static int rcu_wdog_ctr;	/* time since last end-of-batch, in usecs */
static int rcu_wdog_lim = 2 * USEC_PER_SEC;	/* rcu watchdog interval */

/*
 * Invoke whenever the calling CPU consents to end-of-batch.  All CPUs
 * must so consent before the batch is truly ended. xchg() forces the
 * store through the write buffer to L1, where it can be snooped by
 * the other cpus, before going on.
 *
 * @cpu - must be the invoking cpu.
 */
static inline void rcu_eob_cpu(int cpu)
{
	struct rcu_data *rd = &rcu_data[cpu];
	xchg(&rd->wait, 0);
}

static inline void rcu_eob(void)
{
	rcu_eob_cpu(smp_processor_id());
}

void jrcu_read_unlock(void)
{
	if (preempt_count() == 1)
		rcu_eob();
	preempt_enable();
}
EXPORT_SYMBOL(jrcu_read_unlock);

/*
 * Tap into irq_exit.
 *
 * This marks the cpu as agreeing to end-of-batch if the code the interrupt
 * driver is returning to is at a quiescent point.
 */
void rcu_irq_exit(void)
{
	int cpu = smp_processor_id();

	/*
	 * rcu_irq_exit is called with preemption blocked,
	 * the -1, below, adjusts for this.  The test against
	 * idle_cpu() is necessary because the idle task
	 * runs in voluntary preemption mode (ie, its base
	 * preempt count is '1' not '0').
	 */
	if ((preempt_count() - 1) <= idle_cpu(cpu))
		rcu_eob_cpu(cpu);
}

void rcu_note_context_switch(int cpu)
{
	rcu_eob_cpu(cpu);
}
EXPORT_SYMBOL_GPL(rcu_note_context_switch);

void rcu_note_might_resched(void)
{
	preempt_disable();
	rcu_eob();
	preempt_enable();
}
EXPORT_SYMBOL(rcu_note_might_resched);


struct rcu_synchronize {
	struct rcu_head head;
	struct completion completion;
};

static void wakeme_after_rcu(struct rcu_head  *head)
{
	struct rcu_synchronize *rcu;

	rcu = container_of(head, struct rcu_synchronize, head);
	complete(&rcu->completion);
}

/*
 * A pair of calls to mark regions in time where framing has to speed up.
 * These calls nest.
 */
static atomic_t rcu_expedite;
static struct task_struct *rcu_daemon;

static void rcu_expedite_start(void)
{
	int new = atomic_inc_return(&rcu_expedite);
	if (new == 1 && rcu_daemon)
		wake_up_process(rcu_daemon);
}

static void rcu_expedite_stop(void)
{
	atomic_dec(&rcu_expedite);
}

static int rcu_frame_rate_usecs(void)
{
	return atomic_read(&rcu_expedite)
		? rcu_period_us_expedited : rcu_period_us;
}

void synchronize_sched(void)
{
	struct rcu_synchronize rcu;

	if (!rcu_scheduler_active)
		return;

	init_completion(&rcu.completion);
	call_rcu_sched(&rcu.head, wakeme_after_rcu);
	rcu_expedite_start();
	wait_for_completion(&rcu.completion);
	rcu_expedite_stop();
	atomic_inc(&rcu_stats.nsyncs);

}
EXPORT_SYMBOL_GPL(synchronize_sched);

void rcu_barrier(void)
{
	/*
	 * A pair of synchronize_sched's works only because of a fluke
	 * of implementation: no callback in some newer batch is retired
	 * until all callbacks in earlier batches are retired.
	 */
	synchronize_sched();
	synchronize_sched();
	atomic_inc(&rcu_stats.nbarriers);
}
EXPORT_SYMBOL_GPL(rcu_barrier);

/*
 * Insert an RCU callback onto the calling CPUs list of 'current batch'
 * callbacks.
 */
void call_rcu_sched(struct rcu_head *cb, void (*func)(struct rcu_head *rcu))
{
	unsigned long flags;
	struct rcu_data *rd;
	struct rcu_list *clist;

	cb->func = func;
	cb->next = NULL;

	raw_local_irq_save(flags);
	rd = &rcu_data[smp_processor_id()];
	raw_spin_lock(&rd->lock);

	clist = &rd->clist;
	rcu_list_add(clist, cb);
	rd->nqueued++;
	raw_spin_unlock(&rd->lock);
	raw_local_irq_restore(flags);
}
EXPORT_SYMBOL_GPL(call_rcu_sched);

/*
 * Invoke all callbacks on the passed-in list.
 */
static void rcu_invoke_callbacks(struct rcu_list *pending)
{
	struct rcu_head *curr, *next;

	for (curr = pending->head; curr;) {
		unsigned long offset = (unsigned long)curr->func;
		next = curr->next;
		if (__is_kfree_rcu_offset(offset))
			kfree((void *)curr - offset);
		else
			curr->func(curr);
		curr = next;
		rcu_stats.ninvoked++;
	}
}

/*
 * Check if the conditions for ending the current batch are true. If
 * so then end it.
 *
 * Must be invoked periodically.  There are no restriction on how often
 * other than considerations of efficiency.
 */
static void __rcu_delimit_batches(struct rcu_list *pending)
{
	struct rcu_data *rd;
	struct rcu_list *clist, *plist;
	int cpu, eob;

	if (!rcu_scheduler_active)
		return;

	rcu_stats.nlast++;

	/*
	 * Find out if the current batch has ended.
	 */
	eob = 1; /* first assume that all cpus will allow end-of-batch */
	for_each_online_cpu(cpu) {
		rd = &rcu_data[cpu];

		/* we've got nothing to do if this cpu allows end-of-batch */
		smp_rmb();
		if (rd->wait == 0)
			continue;

		/*
		 * Cpu has not yet told us the batch can end.  That might be
		 * because it is 100% idle or 100% in userspace, preventing
		 * it from executing a tap.  Ask the cpu if it is busy or
		 * quiescent right now.
		 */
		if (rcu_iscpu_busy(cpu)) {
			/* a busy cpu forbids the batch to end right now */
			eob = 0;
			break;
		}
		/*
		 * A quiescent cpu allows end-of-batch.	Remember for
		 * later that this cpu said the current batch could end.
		 * This is in case we come across some other cpu that
		 * forbids the batch to end in this frame.
		 */
		xchg(&rd->wait, 0);
	}

	/*
	 * Exit frame if batch has not ended.  But first, tickle all
	 * non-cooperating CPUs if enough time has passed.  The tickle
	 * consists of forcing each cpu to reschedule at the earliest
	 * possible opportunity.
	 */
	if (eob == 0) {
		if (rcu_wdog_ctr >= rcu_wdog_lim) {
			rcu_wdog_ctr = 0;
			rcu_stats.nforced++;
			for_each_online_cpu(cpu) {
				if (rcu_data[cpu].wait)
					smp_send_reschedule(cpu);
			}
		}
		rcu_wdog_ctr += rcu_frame_rate_usecs();
		return;
	}

	/*
	 * End the current RCU batch and start a new one.  This advances the
	 * FIFO of batches one step.  Callbacks that drop off the end of the
	 * FIFO are put (temporarily) into a single, global pending list.
	 * We loop thru the present cpus (rather than the online cpus)
	 * to clean up those cpus that have gone offline.
	 */
	for_each_present_cpu(cpu) {
		rd = &rcu_data[cpu];
		plist = &rd->plist;
		clist = &rd->clist;
		raw_spin_lock(&rd->lock);
		/* chain previous batch of callbacks to the pending list */
		if (plist->head) {
			rcu_list_join(pending, plist);
			rcu_list_init(plist);
		}
		/* chain the current batch of callbacks to the previous list */
		if (clist->head) {
			rcu_list_join(plist, clist);
			rcu_list_init(clist);
		}
		raw_spin_unlock(&rd->lock);
		/*
		 * Mark this cpu as needing to pass thru a fresh quiescent
		 * point. Offline cpus are considered permanently quiescent.
		 */
		xchg(&rd->wait, !!cpu_online(cpu));
	}

	rcu_stats.nbatches++;
	rcu_stats.nlast = 0;
	rcu_wdog_ctr = 0;
}

/*
 * This is invoked periodically to mark frame boundaries.  It will,
 * however, NOP (not mark a frame boundary) if it was called too soon
 * after the previously established frame boundary.  This guarantees no
 * frame is below a certain size.
 *
 * If this frame boundary is legit, then we go and check if this frame
 * boundary should also be a batch boundary.  If so then we go off and
 * do batch boundary processing -- push the current batch down into
 * the FIFO and start a new, empty current batch, as described in
 * Documentation/RCU/jrcu.txt.
 */
static void rcu_delimit_batches(void)
{
	unsigned long flags;
	struct rcu_list pending;

	raw_local_irq_save(flags);

#ifndef CONFIG_JRCU_DEBUG
	/*
	 * Disable all limits on JRCU frame rate when running
	 * in debug mode.
	 */
	if (1) {
		static ktime_t rcu_prev;
		ktime_t now, delta;

		now = ktime_get();
		delta = ktime_sub(now, rcu_prev);
		if (ktime_to_ns(delta) < rcu_period_ns_min) {
			raw_local_irq_restore(flags);
			return;
		}
		rcu_prev = now;
	}
#endif

	rcu_list_init(&pending);
	rcu_stats.npasses++;

	__rcu_delimit_batches(&pending);
	raw_local_irq_restore(flags);

	if (pending.head)
		rcu_invoke_callbacks(&pending);
}

void rcu_init(void)
{
	int cpu;
	for_each_possible_cpu(cpu)
		raw_spin_lock_init(&rcu_data[cpu].lock);
}

/* ------------------ interrupt driver section ------------------ */

/*
 * We drive RCU from a periodic interrupt only during boot, or
 * if the daemon goes away on us.  This is probably overkill.
 */

#include <linux/time.h>
#include <linux/delay.h>
#include <linux/hrtimer.h>
#include <linux/interrupt.h>

static struct hrtimer rcu_timer;

static void rcu_softirq_func(struct softirq_action *h)
{
	rcu_delimit_batches();
}

static enum hrtimer_restart rcu_timer_func(struct hrtimer *t)
{
	ktime_t next;
	int usecs = rcu_frame_rate_usecs();

	raise_softirq(RCU_SOFTIRQ);

	next = ktime_add_us(ktime_get(), usecs);
	hrtimer_set_expires_range_ns(&rcu_timer, next, 0);
	return HRTIMER_RESTART;
}

static void rcu_timer_start(void)
{
	int nsecs = rcu_period_us * NSEC_PER_USEC;
	hrtimer_forward_now(&rcu_timer, ns_to_ktime(nsecs));
	hrtimer_start_expires(&rcu_timer, HRTIMER_MODE_ABS);
}

static void rcu_timer_stop(void)
{
	hrtimer_cancel(&rcu_timer);
}

static __init void rcu_timer_init(void)
{
	open_softirq(RCU_SOFTIRQ, rcu_softirq_func);

	hrtimer_init(&rcu_timer, CLOCK_MONOTONIC, HRTIMER_MODE_ABS);
	rcu_timer.function = rcu_timer_func;
}

void __init rcu_scheduler_starting(void)
{
	rcu_timer_init();
	rcu_scheduler_active = 1;
	rcu_timer_start();
	pr_info("JRCU: callback processing via timer started.\n");
}

/* ------------------ daemon driver section --------------------- */

/*
 * During normal operation, JRCU state is advanced by this daemon. Using a
 * daemon gives the administrator finer control over the cpu and priority
 * of JRCU callback processing than is possible with an interrupt or
 * softirq driver.
 */
#include <linux/err.h>
#include <linux/param.h>
#include <linux/kthread.h>

static int rcu_priority;

static int jrcu_set_priority(int priority)
{
	struct sched_param param;

	if (priority == 0) {
		set_user_nice(current, -19);
		return 0;
	}

	if (priority < 0)
		param.sched_priority = MAX_USER_RT_PRIO + priority;
	else
		param.sched_priority = priority;

	sched_setscheduler_nocheck(current, SCHED_RR, &param);
	return param.sched_priority;
}

static int jrcud_func(void *arg)
{
	current->flags |= PF_NOFREEZE;
	rcu_priority = jrcu_set_priority(CONFIG_JRCU_DAEMON_PRIO);

	rcu_timer_stop();
	pr_info("JRCU: timer exiting, daemon-based processing started.\n");

	while (!kthread_should_stop()) {
		int usecs = rcu_frame_rate_usecs();
		usleep_range_interruptible(usecs, usecs);
		rcu_delimit_batches();
	}

	rcu_daemon = NULL;
	rcu_timer_start();
	pr_info("JRCU: daemon exiting, timer-based processing restarted.\n");
	return 0;
}

static __init int rcu_start_daemon(void)
{
	struct task_struct *p;

	p = kthread_run(jrcud_func, NULL, "jrcud");
	if (IS_ERR(p)) {
		pr_warn("JRCU: cannot replace callback timer with a daemon\n");
		return -ENODEV;
	}
	rcu_daemon = p;
	return 0;
}
subsys_initcall_sync(rcu_start_daemon);

/* ------------------ debug and statistics section -------------- */

#ifdef CONFIG_DEBUG_FS

#include <linux/debugfs.h>
#include <linux/seq_file.h>

static int rcu_hz = RCU_HZ_DEFAULT;

static int rcu_debugfs_show(struct seq_file *m, void *unused)
{
	int cpu;
	s64 nqueued;

	nqueued = 0;
	for_each_present_cpu(cpu)
		nqueued += rcu_data[cpu].nqueued;

	seq_printf(m, "%14u: hz, %s driven\n",
		rcu_hz, rcu_daemon ? "daemon" : "hrtimer");
	seq_printf(m, "%14u: #barriers\n",
		atomic_read(&rcu_stats.nbarriers));
	seq_printf(m, "%14u: #syncs\n",
		atomic_read(&rcu_stats.nsyncs));

	seq_printf(m, "\n");
	seq_printf(m, "%14u: #passes\n",
		rcu_stats.npasses);
	seq_printf(m, "%14u: #passes resulting in end-of-batch\n",
		rcu_stats.nbatches);
	seq_printf(m, "%14u: #passes not resulting in end-of-batch\n",
		rcu_stats.npasses - rcu_stats.nbatches);
	seq_printf(m, "%14u: #passes forced (0 is best)\n",
		rcu_stats.nforced);
	seq_printf(m, "%14u: #secs before a pass is forced (wdog)\n",
		rcu_wdog_lim / (int)USEC_PER_SEC);

	seq_printf(m, "\n");
	seq_printf(m, "%14llu: #callbacks invoked\n",
		rcu_stats.ninvoked);
	seq_printf(m, "%14d: #callbacks left to invoke\n",
		(int)(nqueued - rcu_stats.ninvoked));
	seq_printf(m, "\n");

	for_each_online_cpu(cpu)
		seq_printf(m, "%4d ", cpu);
	seq_printf(m, "  CPU\n");

	for_each_online_cpu(cpu) {
		struct rcu_data *rd = &rcu_data[cpu];
		seq_printf(m, "--%c%c ",
			idle_cpu(cpu) ? 'I' : '-',
			rd->wait ? 'W' : '-');
	}
	seq_printf(m, "  FLAGS\n");

	for_each_online_cpu(cpu) {
		struct rcu_data *rd = &rcu_data[cpu];
		struct rcu_list *l = &rd->clist;
		seq_printf(m, "%4d ", l->count);
	}
	seq_printf(m, "  curr Q\n");

	for_each_online_cpu(cpu) {
		struct rcu_data *rd = &rcu_data[cpu];
		struct rcu_list *l = &rd->plist;
		seq_printf(m, "%4d ", l->count);
	}
	seq_printf(m, "  prev Q\n");
	seq_printf(m, "\nFLAGS: I - idle, W - waiting for end-of-batch\n");

	return 0;
}

static ssize_t rcu_debugfs_write(struct file *file,
	const char __user *buffer, size_t count, loff_t *ppos)
{
	int i, j, c;
	char token[32];

	if (!capable(CAP_SYS_ADMIN))
		return -EPERM;

	if (count <= 0)
		return count;

	if (!access_ok(VERIFY_READ, buffer, count))
		return -EFAULT;

	i = 0;
	if (__get_user(c, &buffer[i++]))
		return -EFAULT;

next:
	/* Token extractor -- first, skip leading whitepace */
	while (c && isspace(c) && i < count) {
		if (__get_user(c, &buffer[i++]))
			return -EFAULT;
	}

	if (i >= count || c == 0)
		return count;	/* all done, no more tokens */

	j = 0;
	do {
		if (j == (sizeof(token) - 1))
			return -EINVAL;
		token[j++] = c;
		if (__get_user(c, &buffer[i++]))
			return -EFAULT;
	} while (c && !isspace(c) && i < count); /* extract next token */
	token[j++] = 0;

	if (!strncmp(token, "hz=", 3)) {
		int rcu_hz_wanted = -1;
		sscanf(&token[3], "%d", &rcu_hz_wanted);
		if (rcu_hz_wanted < 1)
			return -EINVAL;
#ifndef CONFIG_JRCU_DEBUG
		if (rcu_hz_wanted > 1000)
			return -EINVAL;
		if (USEC_PER_SEC / rcu_hz_wanted < rcu_period_us_expedited)
			return -EINVAL;
#endif
		rcu_hz = rcu_hz_wanted;
		rcu_period_us = USEC_PER_SEC / rcu_hz;
	} else if (!strncmp(token, "wdog=", 5)) {
		int wdog = -1;
		sscanf(&token[5], "%d", &wdog);
		if (wdog < 1 || wdog > 300)
			return -EINVAL;
		rcu_wdog_lim = wdog * USEC_PER_SEC;
#ifdef CONFIG_JRCU_DEBUG
	} else if (!strncmp(token, "test=", 5)) {
		u64 start, nsecs;
		int msecs = -1;
		sscanf(&token[5], "%d", &msecs);
		if (msecs < 0 || msecs > 3500)
			return -EINVAL;
		nsecs = msecs * NSEC_PER_MSEC;
		preempt_disable();
		start = sched_clock();
		while ((sched_clock() - start) < nsecs)
			cpu_relax();
		preempt_enable();
#endif
	} else
		return -EINVAL;
	goto next;
}

static int rcu_debugfs_open(struct inode *inode, struct file *file)
{
	return single_open(file, rcu_debugfs_show, NULL);
}

static const struct file_operations rcu_debugfs_fops = {
	.owner = THIS_MODULE,
	.open = rcu_debugfs_open,
	.read = seq_read,
	.write = rcu_debugfs_write,
	.llseek = seq_lseek,
	.release = single_release,
};

static struct dentry *rcudir;

static int __init rcu_debugfs_init(void)
{
	struct dentry *retval;

	rcudir = debugfs_create_dir("rcu", NULL);
	if (!rcudir)
		goto error;

	retval = debugfs_create_file("rcudata", 0644, rcudir,
			NULL, &rcu_debugfs_fops);
	if (!retval)
		goto error;

	return 0;

error:
	debugfs_remove_recursive(rcudir);
	pr_warn("JRCU: Could not create debugfs files.\n");
	return -ENOSYS;
}
late_initcall(rcu_debugfs_init);
#endif /* CONFIG_DEBUG_FS */
